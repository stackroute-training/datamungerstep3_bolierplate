namespace DbEngine.query
{
    /*
	 This class should contain a member variable which is a String array, to hold
	 the data type for all columns for all data types
	*/
    public class DataTypeDefinitions
    {
        public string[] DataTypes { get; set; }

        //implement constructor and override tostring method
        public DataTypeDefinitions(string id, string season, string city, string date, string team1, string team2, string tossWinner, string tDesci, string result, string dl_applied, string winner, string winByRuns, string winByWick, string pom, string venue, string u1, string u2, string u3)
        {
            DataTypes = new[] { id, season, city, date, team1, team2, tossWinner, tDesci, result, dl_applied, winner, winByRuns, winByWick, pom, venue, u1, u2, u3 };
        }
    }
}