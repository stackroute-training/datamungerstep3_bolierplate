using System;
using System.IO;
using DbEngine.query;

namespace DbEngine.reader
{
    public class CsvQueryProcessor: QueryProcessingEngine
    {
        private readonly string _fileName;
        private StreamReader _reader;

        // Parameterized constructor to initialize filename
        public CsvQueryProcessor(string fileName)
        {
            this._fileName = fileName;
        }

        /*
	    Implementation of getHeader() method. We will have to extract the headers
	    from the first line of the file.
	    Note: Return type of the method will be Header
	    */
        public override Header GetHeader()
        {
            // read the first line
            // populate the header object with the String array containing the header names
            try
            {
                string line;
                using (StreamReader reader = new StreamReader(_fileName))
                {
                    
                    line = reader.ReadLine();
                    reader.Close();
                    string[] parts = line.Split(',');
                    if (parts.Length == 18)
                    {
                        Header header = new Header(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6], parts[7], parts[8], parts[9], parts[10], parts[11], parts[12], parts[13], parts[14], parts[15], parts[16], parts[17]);
                        return header;
                    }

                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /*
	    Implementation of getColumnType() method. To find out the data types, we will
	    read the first line from the file and extract the field values from it. If a
	    specific field value can be converted to Integer, the data type of that field
	    will contain "System.Int32", otherwise if it can be converted to Double,
	    then the data type of that field will contain "System.Double", otherwise,
	    the field is to be treated as String. 
	     Note: Return Type of the method will be DataTypeDefinitions
	 */
        public override DataTypeDefinitions GetColumnType() 
        {
            try
            {
                string line;
                using (StreamReader read = new StreamReader(_fileName))
                {
                    line = read.ReadLine();
                    string[] parts = line.Split(',');
                    string[] parts2 = new string[parts.Length];

                    for (int i = 0; i < parts.Length; i++)
                    {
                        if (parts[i] == "id" || parts[i] == "season" || parts[i] == "dl_applied" || parts[i] == "win_by_runs" || parts[i] == "win_by_wickets")
                        {
                           
                            parts2[i] = typeof(int).ToString();

                        }
                        else
                        {
                            Type t = parts[i].GetType();
                            parts2[i] = t.ToString();
                        }
                    }

                    DataTypeDefinitions data = new DataTypeDefinitions(parts2[0], parts2[1], parts2[2], parts2[3], parts2[4], parts2[5], parts2[6], parts2[7], parts2[8], parts2[9], parts2[10], parts2[11], parts2[12], parts2[13], parts2[14], parts2[15], parts2[16], parts2[17]);
                    return data;
                    

                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

         //getDataRow() method will be used in the upcoming assignments
        public override void GetDataRow()
        {

        }
    }
}